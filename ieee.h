enum {
	MAClen = 6
};

typedef enum {
	IBSSmode = 0x0,
	TOAPmode = 0x1,
	FROMAPmode = 0x2,
	WDSmode = 0x3
} Dsmode;

typedef enum {
	DSmask = 0x0003
} Framectlmasks;


/* adapted from a wireshark thingy */
struct radiohdr {
	uint8_t it_version;	/* Version 0. Only increases
				 * for drastic changes,
				 * introduction of compatible
				 * new fields does not count.
				 */
	uint8_t it_pad;
	uint16_t it_len;       	/* length of the whole
				 * header in bytes, including
				 * it_version, it_pad,
				 * it_len, and data fields.
				 */
	uint32_t it_present;	/* A bitmap telling which
				 * fields are present. Set bit 31
				 * (0x80000000) to extend the
				 * bitmap by another 32 bits.
				 * Additional extensions are made
				 * by setting bit 31.
				 */
};

/* http://www.itcertnotes.com/2011/05/ieee-80211-frame-types.html */
struct wifihdr {
	uint8_t miscinfo;
	uint8_t ctlflags;
	uint16_t duration;
	union {
		struct {
			uint8_t da[MAClen];
			uint8_t sa[MAClen];
			uint8_t bssid[MAClen];
			uint16_t seqctl;
			uint8_t unused[MAClen];
		} ibss;
		struct {
			uint8_t da[MAClen];
			uint8_t bssid[MAClen];
			uint8_t sa[MAClen];
			uint16_t seqctl;
			uint8_t unused[MAClen];
		} fromap;
		struct {
			uint8_t bssid[MAClen];
			uint8_t sa[MAClen];
			uint8_t da[MAClen];
			uint16_t seqctl;
			uint8_t unused[MAClen];
		} toap;
		struct {
			uint8_t ra[MAClen];
			uint8_t ta[MAClen];
			uint8_t da[MAClen];
			uint16_t seqctl;
			uint8_t sa[MAClen];
		} wds;
	} dsmode;
};
