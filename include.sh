script0="$0"
basedir="$(dirname "$script0")"
PATH="$basedir:$PATH"

USAGEDIR="$basedir/usage"

HC="[0123456789abcdefABCDEF]"
MAC="$HC$HC:$HC$HC:$HC$HC:$HC$HC:$HC$HC:$HC$HC"
USAGEMIN="1.0"


function die { echo "ERROR: $@" 2>&1; exit -1; }
