#!/usr/bin/env ksh

. `dirname $0`/include.sh


mc="$1"


cat <<'__EOF__'
<html>
<style>
body {
	margin: 0;
	padding: 0;
	font-family: Helvetica, Verdana, Arial, 'Liberation Sans', FreeSans, sans-serif;
}

pre {
	font-family: Helvetica, Verdana, Arial, 'Liberation Sans', FreeSans, sans-serif;
	font-style: italic;
	display: inline;
}

#header {
	color: #fff;
	background-color: #069;
	border: solid 0 black;
	border-width: 2px 0;
}

#content {
	padding: 4em;
	padding: 0.25em 0.25em 0.25em 0.5em;
	border-left: 1px solid #ddd;
	display:block;
	overflow: hidden;
	height: auto;
}

#sidebar {
	float: left;
	clear: left;
	width: 20em;
	margin: 0 0 0 0;
	padding: 0.25em 0.25em 0.25em 0.5em;
	text-align: left;
	position: relative;
	border-bottom: 1px solid #ddd;
}

.mac {
	position: relative;
	float: left;
	width: 100%;
	z-index: 100;
	font-weight: bold;
}
.macback {
	height: 20%;
	width: 100%;
	top: 80%;
	//left: 0;
	background: #00ff00;
	position: absolute;
	z-index: -1;
}
.usage {
	float: left;
	text-align: right;
	width: 30%;
}
.input {
	float: left;
}
.dateform {
	overflow: hidden;
}
.datefield {
	width: 20%;
	float: left;
	margin: 0 0 0 3%;
}
#submit {
	//width: 100%
	align: right;
	float: right;
}
</style>
<body>
<div id="header">
<h1 style="text-align:center">wifi usage report</h1>
</div>
<form name="input" action="display" method="get">
<div id="sidebar">
	<div id="macselect">
		<h3>Data view</h3>
__EOF__

((i = 0));
awk -v findmacs="$(which findmacs.sh)" -v OFS=$'\t' '
	BEGIN {
		while(findmacs | getline) ms[$1] = $2;
		close(findmacs);
		}
	/^/ {
		split($1, a, "@");
		print $1, $2, $3, \
			ms[a[1]] ? ms[a[1]] : a[1],
			ms[a[2]] ? ms[a[2]] : a[2];
	}' |
while IFS=$'\t' read -r m u p a1 a2; do
	cat <<__EOF__
	<p>
		<div class="mac">
			<span class="macback" style="width: $p%"></span>
			${a1}@${a2}
		</div>
	$m<br/>
	${u}M bits
	</p>
__EOF__
((i++)); done

cat <<'__EOF__'
	</div>
</div>


<div id="content">

<div id="chart">
	<h3>Mac addresses</h3>
	<img src="chart.png"/>
</div>
<h3>Days</h3>
<table>
__EOF__

((w = 30))
((i = 0))
(cd "$USAGEDIR";
find . -type d -mindepth 3) | sort |
while IFS="/" read -r x y m d; do
	((i++ == 0)) && echo "<tr>"

	cat <<__EOF__
	<td><a href="display?start-year=$y&start-month=$m&start-day=$d&end-year=$y&end-month=$m&end-day=$((d+1))">$y/$m/$d</a></td>
__EOF__

	((i >= w)) && echo "</tr>" && ((i = 0))
done

cat <<'__EOF__'
</table>
</div>
</form>
</body>
</html>
__EOF__
