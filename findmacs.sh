. `dirname $0`/include.sh

MACLIST="$(dirname "$script0")/maclist"

function thetatlist {
	typeset CONFSITE="http://192.168.1.254/xslt?PAGE=C_2_0"
 	typeset -a info
	
	# info extracted should alternate name1, mac1, name1, mac1, etc.
	IFS=$'\n' info=($(wget --quiet "$CONFSITE" -O - | \
		sed -ne '/<th.*MAC Address.*th>/,/<\/table>/ {
			s/^[^<]*<td class="rowlabel"[^>]*>\(.*\)<.td>$/\1/p
			s/^[^<]*<td[^>]*>\(..:..:..:..:..:..\)<.td>$/\1/p
		}'))
	
	while ((i+1 < ${#info[@]})); do
		echo -e "${info[i+1]}\t${info[i]}"
	((i+=2)); done
}


if [[ $1 == "update" ]]; then
	echo "updating mac list..."
	f=$(mktemp)
	(thetatlist; cat "$MACLIST") | sort -u -k 2 > "$f"
	mv "$f" "$MACLIST"
fi

cat "$MACLIST"
