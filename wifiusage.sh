#!/usr/bin/env ksh

. `dirname $0`/include.sh


function totalusage {
	typeset s="$1"
	typeset e="$2"
	typeset data=$(mktemp)
	
	if [[ -n "$1" ]]; then
		s="$(date -d "$1" "+%s")" || die "invalid date format!"
	fi
	if [[ -n "$2" ]]; then
		e="$(date -d "$2" "+%s")" || die "invalid date format!"
	fi
	
	echo "S: $(date -d @$s) E: $(date -d @$e)" 1>&2
	
	find "$USAGEDIR" -name "$MAC@$MAC" | awk -v data="$data" -v s="$s" \
		-v e="$e" -v minpct="$USAGEMIN" '
		/^/ {
			p = $0;
			match(p, "[^/]*$");
			u = substr(p, RSTART, RLENGTH);
			
			while(getline < p) {
				if(s && int($1) < int(s)) {continue;}
				if(e && int($1) > int(e)) {break;}
				usages[u] += $2;
				max = max < usages[u] ? usages[u] : max;
			}
		}
		END {
			for(u in usages) {
				pct = (100.0 * usages[u]) / max;
				usages[u] /= 1e+6;
				if(pct < minpct) {continue};
				printf "%s\t%0.2f\t%02.f\n", u, usages[u], pct;
			}
		}' | sort -r -n -k 2

	typeset pct="$(echo "2k 100 ${u}* ${max}/p" | dc)"
}

function savepdump {
	while IFS=$'\t' read "time" "usage" "user"; do
		typeset d="$USAGEDIR/$(date -d "@$time" +"%Y/%m/%d")"
		
		mkdir -p "$d"
		echo -e "$time\t$usage" >> "$d/$user"
	done
}

function makeplot {
	typeset data="$(mktemp)"
	typeset s=""
	typeset e=""
	
	if [[ -n "$1" ]]; then
		s="$(date -d "$1" +%s)" || die "invalid date format!"
	fi
	if [[ -n "$2" ]]; then
		e="$(date -d "$2" +%s)" || die "invalid date format!"
	fi
	shift 2
	
	while IFS=$'\n' read m; do
		find "$USAGEDIR" -name "$m"
	done | awk -v data="$data" -v s="$s" -v e="$e" \
		-v findmacs="$(which findmacs.sh)" -v OFS=$'\t' '
		BEGIN {
			i = 0;
			cur = "";
			
			print "set xdata time";
			print "set xtics rotate right"
			print "set timefmt \"%s\"";
			print "set format y \"%0.0s %c\"";
			print "set format x \"%F %H:%M:%S\"";
			#print "set term svg size 900, 700 fixed"
			#print "set term dumb size 160 50 enhanced"
			print "set term png size 1024 768";
			print "set output"
			
			printf "plot ";

			while(findmacs | getline) macs[$1] = $2;
			close(findmacs);
		}
		/^/ {
			p = $0;
			match(p, "[^/]*$");
			u = substr(p, RSTART, RLENGTH);
			split(u, um, "@");
			
			if(u != cur) {
				if (hasdata) {
					printf "\n\n" > data;
					
					printf "%s\\\n", (i==0 ? "" : ", ");
					printf "\"%s\" using 1:2 ", data;
					printf "index %d title \"%s@%s\" ", i++, \
						macs[um[1]] ? macs[um[1]] : um[1], \
						macs[um[2]] ? macs[um[2]] : um[2];
				}
				
				cur = u;
				hasdata = 0;
			}
			while(getline <p) {
				print "true" >/dev/stderr;
				if(s && int($1) < int(s)) {continue;}
				if(e && int($1) > int(e)) {break;}
				print $0 > data;
				hasdata = 1;
			}
		}
		END {
			printf "\n";
		}' | gnuplot
	rm -f "$data"
}

# adapated from several places on the internet...but didn't work. Needed to add
#     the little bit of &3 magic (per http://mywiki.wooledge.org/BashFAQ/085)
function httpserver {
	typeset fifo=$(mktemp) && rm "$fifo" && mkfifo "$fifo"
	typeset reqhead
	
	while true; do
		cat "$fifo" |
		nc -l 8888 | (
			exec 3>"$fifo"
			IFS= read reqhead
			httpresp "$reqhead" >&3
		)
		exec 3>&-
		wait
	done
}

function httpresp {
	typeset usage=""
	typeset resp=$(mktemp)
	typeset mime="text/html"
	typeset result="200 OK"
	typeset reqhead=""
	
	typeset s="year/month/day hour:minute"
	typeset e="year/month/day hour:minute"
	
	reqhead="${1#GET }"
	reqhead="${reqhead% HTTP*}"
	
	if [[ $reqhead =~ ^/display ]]; then
		usage=$(mktemp)
		
		echo "${reqhead#*\?}" | tr '&' '\n' |
		while IFS="=" read k v; do
			if [[ $k =~ ^start ]]; then
				s="$(echo $s |
					sed -e 's/'"${k#start-}"'/'"$v"'/')"
			elif [[ $k =~ ^end ]]; then
				e="$(echo $e |
					sed -e 's/'"${k#end-}"'/'"$v"'/')"
			elif [[ $k =~ ^mac-count ]]; then
				mc="$v"
			fi
			echo -e "$k\t$v" 1>&2
		done
		
		s=$(date -d "${s%hour*}")
		e=$(date -d "${e%hour*}")
		totalusage "$s" "$e" > "$usage"
		
		cat "$usage" | cut -f 1 |
		makeplot "$s" "$e" > "$USAGEDIR/chart.png"
		
		cat "$usage" | "$basedir"/webinterface.sh "$mc" > "$resp"
	elif [[ $reqhead =~ ^/chart.png ]]; then
		mime="image/png"
	 	cat "$USAGEDIR/chart.png" > "$resp"
	else
		result="404 Not Found"
		echo "not found (404)" > "$resp"
	fi
	
	echo "HTTP/1.0 $result"
	echo "Cache-Control: private"
	echo "Content-Type: $mime"
	echo "Server: ksh-abomination/93"
	echo "Connection: Close"
	echo "Content-Length: $(wc -c "$resp")"
	echo ""
	cat "$resp"

	rm "$resp"
}



if [[ $1 == "listen" ]]; then
	echo "saving data to usage files..."
	trap "kill 0" SIGINT SIGTERM EXIT
	httpserver&
	
	iw dev mon0 info 1>/dev/null 2>/dev/null; typeset s="$?"
	if (($s != 0)); then
		sudo iw dev wlan0 interface add mon0 type monitor
	fi
	sudo ifconfig mon0 up
	
	(sudo pdump mon0) | savepdump
else
	httpserver
fi
