#include <assert.h>

#include <pcap/pcap.h>

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <time.h>

#include "ieee.h"


enum {
	Snaplen = 65535,
	Timeout = 1000,
	Sampleintv = 10
};


struct usage {
	char *user;
	double usage;
	struct usage *n;
} *intvusage = NULL;

struct timespec intvend = {0, 0};


void strfmac(uint8_t mac[MAClen], char str[3*MAClen]){
	uint8_t *mp;
	char *sp;

	mp = mac;
	sp = str;
	while(mp < mac + MAClen) {
		sprintf(sp, "%02x", *(mp++));
		sp += 2;
		*(sp++) = ':';
	}
	*(sp-1) = '\0';
}

void flushusage() {
	struct usage *n, *t;
	
	n = intvusage;
	while(n) {
		printf("%ld\t%f\t%s\n", intvend.tv_sec, n->usage, n->user);
		t = n->n;
		free(n->user);
		free(n);
		n = t;
	}
	intvusage = NULL;
}

void updateusage(char *user, double usage, struct timeval ts) {
	struct usage *n, *t;
	
	if(ts.tv_sec >= intvend.tv_sec) {
		flushusage();
		intvend.tv_sec = 
			ts.tv_sec + Sampleintv - (ts.tv_sec % Sampleintv);
	}

	t = intvusage;
	for(n = intvusage; n; n = n->n) {
		t = n;
		if(!(strcmp(user, n->user))) {
			break;
		}
	}
	
	if(!n) {
		if(!(n = malloc(sizeof(struct usage)))) {
			assert(0 == 1);
		}
		if(!(n->user = strdup(user))) {
			assert(0 == 1);
		}
		n->usage = 0.0;
		n->n = NULL;
		
		if(!t) {
			intvusage = n;
		} else {
			t->n = n;
		}
	}

	n->usage += usage;
}

void handlepkt(u_char *arg, const struct pcap_pkthdr *caphdr,
		const u_char *packet) {
	struct radiohdr *radiohdr;
	struct wifihdr *wifihdr;
	char *user, ms[MAClen*3], bs[MAClen*3];
	
	radiohdr = (struct radiohdr *)packet;
	wifihdr = (struct wifihdr *)(packet + radiohdr->it_len);
	
	switch(wifihdr->ctlflags & DSmask) {
		case IBSSmode:
			updateusage("IBSS", caphdr->len, caphdr->ts);
		break;
		case FROMAPmode:
			strfmac(wifihdr->dsmode.fromap.da, ms);
			strfmac(wifihdr->dsmode.fromap.bssid, bs);
			if(!(user = malloc(strlen(ms) + strlen(bs) + 2))) {
				assert(0 == 1);
			}
			sprintf(user, "%s@%s", ms, bs);
			updateusage(user, caphdr->len, caphdr->ts);
			free(user);
		break;
		case TOAPmode:
			strfmac(wifihdr->dsmode.fromap.sa, ms);
			strfmac(wifihdr->dsmode.fromap.bssid, bs);
			if(!(user = malloc(sizeof(ms) + sizeof(bs) + 1))) {
				assert(0 == 1);
			}
			sprintf(user, "%s@%s", ms, bs);
			updateusage(user, caphdr->len, caphdr->ts);
			free(user);
		break;
		case WDSmode:
			updateusage("WDS", caphdr->len, caphdr->ts);
		break;
		default:
			assert(0 == 1);
	}
}

pcap_t *initdev(char *dev, char *filterstr) {
	char err[PCAP_ERRBUF_SIZE];
	pcap_t *h;
	struct bpf_program filterprog;
	
	if (!(h = pcap_open_live(dev, Snaplen, 1, Timeout, err))) {
		fprintf(stderr, "Couldn't find device: %s\n", err);
		return NULL;
	}
	
	if (pcap_compile(h, &filterprog, filterstr, 1, 0) == -1) {
		fprintf(stderr, "Couldn't compile filter %s. %s\n",
			filterstr, err);
		return NULL;
	}
	
	if (pcap_setfilter(h, &filterprog) == -1) {
	 	fprintf(stderr, "Couldn't apply filter. %s\n", err);
		return NULL;
	}

	return h;
}

int main(int argc, char *argv[]) {
	pcap_t *h;
	/* char err[PCAP_ERRBUF_SIZE];*/
	
	assert(argc == 2); /* yeah... this is how i check my args */
	
	if (!(h = initdev(argv[1], "type data"))) {
	/*if (!(h = pcap_open_offline(argv[1], err))) {*/
		return -1;
	}

	pcap_loop(h, -1, handlepkt, NULL);
	flushusage();
	return 0;
}
